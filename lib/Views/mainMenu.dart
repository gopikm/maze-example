import 'package:flutter/material.dart';

import '../game.dart';
import 'base/baseView.dart';


class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  MazeBallGame game;
  @override
  void initState() {
    super.initState();
    game = MazeBallGame(startView: GameView.MainMenuBackground);
    game.blockResize = true;
  }

  @override
  Widget build(BuildContext context) {
    game.pauseGame = false;
    return GameWidget();
  }
}
